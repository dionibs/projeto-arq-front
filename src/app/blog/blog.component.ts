import { Component, OnInit } from '@angular/core';
export interface BlogTile {
  title: string;
  link: string;
  content: string;
}
@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  news: BlogTile[] = [
    {
      title: 'Notícias globais',
      link: "https://www.globo.com",
      content: "Posts relata como as notícias globais são disponibilizadas ao público."
    },
    {
      title: 'O inesperado acontece em 2021',
      link: "https://www.sbt.com.br/",
      content: "O que ninguém imaginava! Post trata o milagre da transmissão de jogos futibolísticos por canais inimagináveis."
    },
    {
      title: 'Um pouco de tudo.',
      link: "https://www.r7.com/",
      content: "Resumo das séries que move o mundo."
    }
  ];

  constructor() { }

  ngOnInit(): void {
    
  }


}


